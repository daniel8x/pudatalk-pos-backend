<?php

namespace App\DataLoaders;

use App\Collections\EmployeeCollection;
use App\DomainModels\Employee;
use Carbon\Carbon;
use App\DomainModels\Login;
use App\DomainModels\Address;
use App\DomainModels\Name;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class EmployeeSQL implements EmployeeInterface
{

    public function getAll(): EmployeeCollection
    {


    }

    public function create(Employee $employee)
    {
        $insertEmployeeId = DB::table('employees')->insertGetId(
            [
                'pin' => $employee->getPin(),
                'firstname' => $employee->getName()->getFirstName(),
                'status' => $employee->getStatus(),
                'startdate' => Carbon::parse($employee->getStartDate())
            ]
        );


        $insertRoles = DB::table('role_employee')->insert(
            [
                'employee_id' => $insertEmployeeId,
                'role_id' => $employee->getRoleId()
            ]
        );


        $insertSalon = DB::table('salon_employee')->insert(
            [
                'employee_id' => $insertEmployeeId,
                'salon_id' => $employee->getSalonId()
            ]
        );

        $insertUser = DB::table('users')->insert([
            'employee_id' => $insertEmployeeId,
            'username' => $employee->getLogin()->getUsername(),
            'email' => $employee->getLogin()->getEmail(),
            'password' => bcrypt($employee->getLogin()->getPassword())
        ]);

        $insertPayrate = DB::table('payrate_employee')->insert([
            'employee_id' => $insertEmployeeId,
            'payrate_type_id' => $employee->getPayrateTypeId(),
            'sharing_rate' => $employee->getSharingRate(),
            'check_rate' => $employee->getCheckRate(),
            'bao_rate' => $employee->getBaoRate(),
        ]);

    }

//    public function delete($id)
//    {
//        // TODO: Implement delete() method.
//    }
//
    public function getBySalonID($salonId)
    {
        $getEmployee = DB::select("select * from employees 
                                    join salon_employee
                                    join role_employee
                                    join payrate_employee
                                    join users
                                    where employees.id = salon_employee.employee_id
                                    and employees.id = role_employee.employee_id
                                    and employees.id = payrate_employee.employee_id
                                    and employees.id = users.employee_id
                                    and salon_employee.salon_id = $salonId");

        return $this->EmployeeDTOJson($getEmployee);


    }

    private function EmployeeDTOJson(array $data)
    {
        $employees = [];
        foreach ($data as $each)
        {
            $employees['data'][] = Employee::fromAssoc($this->makeEmployeeObject($each))->toArray();
        }
        return $employees;
    }

    private function makeEmployeeObject($data): array{
        return ([
            'id' => $data->id,
            'pin' => $data->pin,
            'salon_id' => $data->salon_id,
            'role_id' => $data->role_id,
            'login' => Login::fromAssoc($data),
            'name' => Name::fromAssoc($data),
            'payrate_type_id' => $data->payrate_type_id,
            'sharing_rate' => $data->sharing_rate,
            'check_rate' => $data->check_rate,
            'bao_rate' => $data->bao_rate,
            'address' => Address::fromAssoc($data),
            'cellphone' => $data->cellphone,
            'status' => $data->status,
            'startdate' => $data->startdate
        ]);

    }

//
//    public function update(Service $service)
//    {
//        // TODO: Implement update() method.
//    }
}
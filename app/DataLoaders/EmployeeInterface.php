<?php
namespace App\DataLoaders;

use App\Collections\EmployeeCollection;
use App\DomainModels\Employee;

interface EmployeeInterface {
//    public function getAll(): EmployeeCollection;
    public function create(Employee $employee);
//    public function delete($id);
    public function getBySalonID($salonId);
//    public function update(Service $service);
}
<?php
namespace App\Http\Controllers;
use App\EloquentModels\EEmployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Traits\ValidationTrait;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller {
    use ValidationTrait;

    private $rules = [
        'username' => 'required',
        'password' => 'required|min:6'
    ];

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (!$this->validator($credentials, $this->rules)) {
            return response()
                ->json([
                    'code' => 422,
                    'message' => 'Validation failed.',
                    'errors' => $this->validationErrors()
                ], 422);
        }

        try {
            $attempt = Auth::attempt($credentials);

            $employeeId = DB::table('users')->select('employee_id')->where('id',Auth::id())->value('employee_id');
            $salon_id = DB::table('salon_employee')->select('salon_id')->where('employee_id',$employeeId)->value('salon_id');
            $role_id = DB::table('role_employee')->select('role_id')->where('employee_id', $employeeId)->value('role_id');

            if ($attempt) {

                $emp_info = ['salon_id' => $salon_id, 'role_id' => $role_id];
                $payload = JWTFactory::emp_info($emp_info)->make();
                $token = (string) JWTAuth::encode($payload);
                return response()->json(['code' => 200, 'token' => $token],200);
            } else {
                return response()->json(['code' => 401, 'message' => 'Invalid credentials.'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['code' => 500, 'message' => 'Problem creating token'], 500);
        }

    }
}
<?php
namespace App\Http\Controllers;
use App\DataMappers\EmployeeMapper;
use App\DomainModels\Address;
use App\DomainModels\Employee;
use App\DomainModels\Name;
use App\DomainModels\Login;
use Illuminate\Http\Request;


class EmployeeController extends Controller {
    private $employeeMapper;

    public function __construct()
    {
        $this->employeeMapper = resolve(EmployeeMapper::class);

    }
    public function create(Request $request)
    {
        return $this->employeeMapper->create($this->employeeDTO($request));
    }

    public function show($salonId){

        return $this->employeeMapper->show($salonId);
    }

    private function employeeDTO($data)
    {
        return new Employee(
            '',
            $data->pin,
            $data->salon_id,
            $data->role_id,
            Login::fromAssoc($data),
            Name::fromAssoc($data),
            $data->payrate_type_id,
            $data->sharing_rate,
            $data->check_rate,
            $data->bao_rate,
            $data->cellphone,
            Address::fromAssoc($data),
            $data->status,
            $data->start_date
        );
    }
}
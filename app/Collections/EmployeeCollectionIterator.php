<?php

namespace App\Collections;
use App\DomainModels\Employee;

class EmployeeCollectionIterator implements \Iterator
{
    private $current = 0;

    /**
     * @var Employee[]
     */
    private $employees;

    public function __construct(EmployeeCollection $employeeCollection)
    {
        $this->employees = $employeeCollection->toArray();
    }

    public function current(): Employee
    {
        return $this->employees[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->employees[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}
<?php

namespace App\Collections;

use App\DomainModels\Employee;

class EmployeeCollection implements \Countable, \IteratorAggregate
{
    /** @var  Employees[] */
    private $employees;

    public function __construct(array $employees)
    {
        $this->employees = $employees;
    }

    public function getIterator(): EmployeeCollectionIterator
    {
        return new EmployeeCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->employees);
    }

    public function toArray(): array
    {
        return $this->employees;
    }

    public function toJson()
    {
        $employeeJson = [];
        foreach ($this->getIterator() as $employee)
        {
            $employeeJson[] = $employee->toJson();
        }
        return $employeeJson;
    }

    /*
    * TODO : Not comfortable with this method
    * will do it later when have time
    */
//    public function toAllArray(): array
//    {
//        $accountsJson = [];
//        foreach ($this->getIterator() as $account)
//        {
//            $accountsJson[] = $account->toArray();
//        }
//        return $accountsJson;
//    }

}
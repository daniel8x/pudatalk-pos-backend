<?php
namespace App\DomainModels;

class Employee
{
    private $id;
    private $pin;
    private $salon_id;
    private $role_id;
    private $login;
    private $name;
    private $payrate_type_id;
    private $sharing_rate;
    private $check_rate;
    private $bao_rate;
    private $cellphone;
    private $address;
    private $status;
    private $start_date;

    /**
     * Employee constructor.
     * @param $id
     * @param $salon_id
     * @param $role_id
     * @param $login
     * @param $name
     * @param $payrate_type_id
     * @param $sharing_rate
     * @param $check_rate
     * @param $bao_rate
     * @param $cellphone
     * @param $address
     * @param $status
     * @param $start_date
     */
    public function __construct($id, $pin, $salon_id, $role_id, Login $login, Name $name, $payrate_type_id, $sharing_rate, $check_rate, $bao_rate, $cellphone,Address $address, $status, $start_date)
    {
        $this->id = $id;
        $this->pin = $pin;
        $this->salon_id = $salon_id;
        $this->role_id = $role_id;
        $this->login = $login;
        $this->name = $name;
        $this->payrate_type_id = $payrate_type_id;
        $this->sharing_rate = $sharing_rate;
        $this->check_rate = $check_rate;
        $this->bao_rate = $bao_rate;
        $this->cellphone = $cellphone;
        $this->address = $address;
        $this->status = $status;
        $this->start_date = $start_date;
    }

    public static function fromAssoc(array $data): Employee
    {
        $id = $data['id'] ?? null;
        $pin = $data['pin'] ?? null;
        $salon_id = $data['salon_id'] ?? null;
        $role_id = $data['role_id'] ?? null;
        $login = $data['login'] ?? null;
        $name = $data['name'] ?? null;
        $payrate_type_id = $data['payrate_type_id'] ?? null;
        $sharing_rate = $data['sharing_rate'] ?? 0;
        $check_rate = $data['check_rate'] ?? 0;
        $bao_rate = $data['bao_rate'] ?? 0;
        $cellphone = $data['cellphone'] ?? null;
        $address = $data['address'] ?? null;
        $status = $data['status'] ?? 1;
        $start_date = $data['startdate'] ?? null;
        return new Employee($id, $pin, $salon_id, $role_id, $login, $name , $payrate_type_id, $sharing_rate,$check_rate, $bao_rate, $cellphone, $address, $status, $start_date);

    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @return mixed
     */
    public function getSalonId()
    {
        return $this->salon_id;
    }

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @return mixed
     */
    public function getLogin(): Login
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPayrateTypeId()
    {
        return $this->payrate_type_id;
    }

    /**
     * @return mixed
     */
    public function getSharingRate()
    {
        return $this->sharing_rate;
    }

    /**
     * @return mixed
     */
    public function getCheckRate()
    {
        return $this->check_rate;
    }

    /**
     * @return mixed
     */
    public function getBaoRate()
    {
        return $this->bao_rate;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'pin' => $this->getPin(),
            'salon_id' => $this->getSalonId(),
            'role_id' => $this->getRoleId(),
            'login' => $this->getLogin()->toArray(),
            'name' => $this->getName()->toArray(),
            'payrate_type_id' => $this->getPayrateTypeId(),
            'sharing_rate' => $this->getSharingRate(),
            'bao_rate' => $this->getBaoRate(),
            'cellphone' => $this->getCellphone(),
            'address' => $this->getAddress()->toArray(),
            'status' => $this->getStatus(),
            'start_date' => $this->getStartDate()
        ];
    }
    public function toJson()
    {
        return json_encode($this->toArray());

    }




}
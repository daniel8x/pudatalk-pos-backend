<?php

namespace App\DomainModels;


class Name
{
    /** @var  string */
    private $firstName;

    /** @var  string */
    private $lastName;

    /** @var  string */
    private $middleName;

    /**
     * Name constructor.
     * @param string $firstName
     * @param string $lastName
     * @param string $middleName
     */
    public function __construct(string $firstName, string $lastName, string $middleName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
    }

    public static function fromAssoc($data): Name
    {
        $firstName = $data->firstname ?? '';
        $lastName = $data->lastname ?? '';
        $middleName = $data->middlename ?? '';

        return  new Name($firstName, $lastName, $middleName);
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function toArray()
    {
        return [
            'firstname' => $this->firstName,
            'middlename' => $this->middleName,
            'lastname' => $this->lastName
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}


class NameNull extends Name
{
    public function __construct(){}
}
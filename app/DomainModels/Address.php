<?php

namespace App\DomainModels;

class Address
{

    /**@var string */
    private $address1;

    /**@var string */
    private $address2;

    /**@var string */
    private $city;

    /**@var string */
    private $state;

    /**@var string */
    private $zipcode;

    /**@var string */
    private $country;

    /**
     * Address constructor.
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param string $zipcode
     * @param string $country
     */
    public function __construct($address1, $address2, $city, $state, $zipcode, $country = 'US')
    {
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->city = $city;
        $this->state = $state;
        $this->zipcode = $zipcode;
        $this->country = $country;
    }


    public static function fromAssoc($data): Address
    {
        $address1 = $data->address1 ?? '';
        $address2 = $data->address2 ?? '';
        $city = $data->city ?? '';
        $state = $data->state ?? '';
        $zipcode = $data->zipcode ?? '';
        $country = $data->country ?? 'US';


        return new Address($address1, $address2, $city, $state, $zipcode, $country);
    }

    /**
     * @return string
     */
    public function getAddress1(): string
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1(string $address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getAddress2(): string
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2(string $address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode(string $zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    public function toArray()
    {
        return [
            'address1' => $this->address1,
            'address2' => $this->address2,
            'city' => $this->city,
            'state' => $this->state,
            'zipcode' => $this->zipcode,
            'country' => $this->country
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }



}

<?php

namespace App\DataMappers;

//use App\Collections\AgentCollection;
use App\DomainModels\Employee;
use App\DataLoaders\EmployeeInterface;

class EmployeeMapper
{

    private $employeeLoader;

    public function __construct(EmployeeInterface $employeeLoader)
    {
        $this->employeeLoader = $employeeLoader;
    }

        public function create(Employee $employee)
    {
        return $this->employeeLoader->create($employee);
    }

    public function show ($salonId)
    {
        return $this->employeeLoader->getBySalonID($salonId);
    }

//    public function getAll(): AgentCollection
//    {
//        return $this->agentLoader->getAll();
//    }
}
<?php

namespace App\EloquentModels;


use Illuminate\Database\Eloquent\Model;

class ERole extends Model
{

    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description',
    ];
}

<?php

use Illuminate\Http\Request;
// CORS
header('Access-Control-Allow-Origin: http://localhost:4200');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/employee', function (Request $request) {
//    return \App\EloquentModels\EEmployee::all();
//});

Route::post('auth/login', 'AuthController@login');
//Route::get('employees','EmployeeController@create');


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('employees/{salonId}','EmployeeController@show');
    Route::post('employees','EmployeeController@create');
    Route::put('employees/{id}','EmployeeController@edit');
    Route::delete('employees/{id}','EmployeeController@delete');
});

